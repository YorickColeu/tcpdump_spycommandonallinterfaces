#!/bin/sh

# Version
VERSION="1.0.0"
VERSION_DATE="2018-01-25"

# Default parameters
IS_MANDATORY_SET=false
WAITING_TIME=1000
OUTPUT_PATH=$PWD
FILE_PREFIX="tcpdump_"
FORCE_OVERWRITE=false
VERBOSE=false

# Help / Usage message
function showHelp {
    echo " "
    echo "tcpdump_spycommandonallinterfaces - For all available interfaces: Start a tcpdump capture, send a command, wait, then stop the capture."
    echo " "
    echo "options:"
    echo "-h, --help                      show this brief help"
    echo "-c, --command=\"COMMAND\"         specify the command to spy"
    echo "-w, --wait=WAITING_TIME         [optional] number of milliseconds to wait after the command was sent before to stop tcpdump acquisition (default: 1000 ms)"
    echo "-o, --output-path=OUTPUT_PATH   [optional] specify a directory path to store output in (default: current directory)"
    echo "-p, --prefix=FILE_PREFIX        [optional] specify a prefix for files that will be created (default: \"tcpdump_\")"
    echo "-f, --force                     [optional] force overwrite if files exist"
    echo "-v, --verbose                   [optional] verbose mode"
    echo "--version                       [optional] Show current version"
    echo " "
}

while [[ "${1:0:1}" = "-" || "$1" =~ ^\+([0-9]+)$ ]]; do
    case $1 in
        -)
            break
            ;;
        --)
            shift
            break
            ;;
        -c| --command)
            COMMAND=$2
            IS_MANDATORY_SET=true
            shift
            ;;
        -w|--wait)
            WAITING_TIME=$2
            shift
            ;;
        -o| --output-path)
            OUTPUT_PATH+=/$2
            shift
            ;;
        -p| --prefix)
            FILE_PREFIX=$2
            shift
            ;;
        -f| --force)
            FORCE_OVERWRITE=true
            ;;
        -v|--verbose)
            VERBOSE=true
            ;;
        --version)
            echo v$VERSION: $VERSION_DATE
            exit 1
            ;;
        -h|-\?|--help)
            showHelp
            exit 1
            ;;
        +[0-9]*)
            selection=${1:1}
            ;;
        *)
            showHelp
            exit 1
            ;;
    esac

    shift
done

if [ $VERBOSE == true ]; then
    echo "COMMAND:" $COMMAND
    echo "WAITING_TIME:" $WAITING_TIME
    echo "OUTPUT_PATH:" $OUTPUT_PATH
    echo "FILE_PREFIX:" $FILE_PREFIX
    echo "FORCE_OVERWRITE:" $FORCE_OVERWRITE
    echo "VERBOSE:" $VERBOSE
fi

if [ $IS_MANDATORY_SET == false ]; then
    echo " "
    echo "ERROR: Missing mandatory argument COMMAND (-c):"
    showHelp
    exit 1
fi

# Get the list of tcpdump interfaces
if hash tcpdump 2>/dev/null; then
    TCPDUMP_D_OUTPUT=`tcpdump -D`
else
    echo "ERROR: tcpdump command not available"
    exit 1
fi

NB_INTERFACE=0;
for j in `echo "$TCPDUMP_D_OUTPUT" | sed 's/.*\.//' | sed -e 's/ .*$//'` 
do
    array[$NB_INTERFACE]=$j; 
    NB_INTERFACE=$(($NB_INTERFACE+1));    
done

if [ $VERBOSE == true ]; then
    echo "tcpdump -D output"
    echo "$TCPDUMP_D_OUTPUT"
    echo "Number of interfaces: " $NB_INTERFACE
    echo "Parsed output:"
    for i in `seq 0 $(($NB_INTERFACE-1))`
    do
        echo "Interface $i: ${array[$i]}"
    done
fi

for i in `seq 0 $(($NB_INTERFACE-1))`
do
    CURRENT_FILE_PATH=$OUTPUT_PATH/$FILE_PREFIX${array[$i]}
    if [ ! -f "$CURRENT_FILE_PATH" ] || [ "$FORCE_OVERWRITE" = true ]
    then
        tcpdump -n -e -vvv -A -i ${array[$i]} > $CURRENT_FILE_PATH &
        sleep $(($WAITING_TIME/1000))
        $COMMAND
        sleep $(($WAITING_TIME/1000))
        kill -9 $(pidof tcpdump)
    else
        echo "ERROR: File $CURRENT_FILE_PATH exists: Use -f option to overwrite"
        showHelp
        exit 1
    fi

done

